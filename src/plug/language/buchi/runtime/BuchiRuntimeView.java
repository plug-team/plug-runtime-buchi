package plug.language.buchi.runtime;

import plug.core.IRuntimeView;
import plug.core.view.ConfigurationItem;
import properties.BuchiAutomata.BuchiAutomataModel.GuardedTransition;
import properties.PropositionalLogic.transformations.PrettyPrinterInline;

import java.util.Collections;
import java.util.List;

public class BuchiRuntimeView implements IRuntimeView<BuchiConfiguration, GuardedTransition> {
    BuchiRuntime runtime;
    public BuchiRuntimeView(BuchiRuntime runtime) {
        this.runtime = runtime;
    }

    @Override
    public BuchiRuntime getRuntime() {
        return runtime;
    }

    @Override
    public List<ConfigurationItem> getConfigurationItems(BuchiConfiguration value) {
        ConfigurationItem buchiState = new ConfigurationItem("state", value.buchiState.getName(), null, null);

        return Collections.singletonList(buchiState);
    }

    @Override
    public String getFireableTransitionDescription(GuardedTransition transition) {
        return transition.getFrom().getName() + "-["+ PrettyPrinterInline.print(transition.getGuard()) +"]->" + transition.getTo().getName() +"\n";
    }
}
