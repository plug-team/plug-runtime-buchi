package plug.language.buchi.runtime;

import plug.core.IBuchiConfiguration;
import plug.core.defaults.DefaultConfiguration;
import properties.BuchiAutomata.BuchiAutomataModel.AcceptingState;
import properties.BuchiAutomata.BuchiAutomataModel.State;

public class BuchiConfiguration extends DefaultConfiguration<BuchiConfiguration> implements IBuchiConfiguration {
    public final State buchiState;

    public BuchiConfiguration(State state) {
        this.buchiState = state;
    }

    @Override
    public BuchiConfiguration createCopy() {
        return new BuchiConfiguration(buchiState);
    }

    @Override
    public int hashCode() {
        return buchiState.getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if(obj instanceof BuchiConfiguration) {
            BuchiConfiguration other = (BuchiConfiguration) obj;
            return buchiState == other.buchiState;
        }
        return false;
    }

    @Override
    public String toString() {
        return buchiState.getName();
    }

    public boolean isAccepting() {
        return buchiState instanceof AcceptingState;
    }
}
