package plug.language.buchi.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import plug.core.IAtomicPropositionsProvider;
import plug.core.IBuchiConfiguration;
import plug.core.IFiredTransition;
import plug.core.ITransitionRelation;
import plug.statespace.transitions.FiredTransition;
import properties.BuchiAutomata.BuchiAutomataModel.Automaton;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.BuchiAutomata.BuchiAutomataModel.GuardedTransition;
import properties.BuchiAutomata.analysis.AtomExtractor;
import properties.PropositionalLogic.PropositionalLogicModel.Atom;
import properties.PropositionalLogic.PropositionalLogicModel.Declaration;
import properties.PropositionalLogic.PropositionalLogicModel.ExpressionDeclaration;

public class BuchiRuntime implements ITransitionRelation<BuchiConfiguration, GuardedTransition>, IAtomicPropositionsProvider {

    private final BuchiDeclaration buchiAutomaton;

    public BuchiRuntime(BuchiDeclaration buchiAutomaton) {
        this.buchiAutomaton = buchiAutomaton;
    }

    Automaton getAutomaton() {
        return (Automaton) buchiAutomaton.getAutomaton();
    }

    @Override
    public Set<BuchiConfiguration> initialConfigurations() {
        return Collections.singleton(new BuchiConfiguration(getAutomaton().getInitial()));
    }

    @Override
    public Collection<GuardedTransition> fireableTransitionsFrom(BuchiConfiguration source) {
        return getAutomaton().getTransitions().stream().filter(t -> t.getFrom() == source.buchiState).collect(Collectors.toList());
    }

    @Override
    public IFiredTransition<BuchiConfiguration, ?> fireOneTransition(BuchiConfiguration source, GuardedTransition transition) {
        return new FiredTransition<BuchiConfiguration, Object>(source, new BuchiConfiguration(transition.getTo()), transition);
    }

    @Override
    public Collection<String> getAtomicPropositions() {

        AtomExtractor extractor = new AtomExtractor();
        List<Atom> atoms = extractor.getAtoms(buchiAutomaton);

        return atoms.stream().map(Atom::getCode).collect(Collectors.toList());
    }
}
