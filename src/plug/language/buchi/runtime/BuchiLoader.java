package plug.language.buchi.runtime;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import org.antlr.v4.runtime.ANTLRInputStream;
import plug.core.ILanguageLoader;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.BuchiAutomata.parser.Parser;

public class BuchiLoader implements ILanguageLoader<BuchiRuntime> {

    @Override
    public BuchiRuntime getRuntime(URI modelURI, Map<String, Object> options) throws IOException {
        try (InputStream stream = new BufferedInputStream(modelURI.toURL().openStream())) {
            BuchiDeclaration declaration = new Parser().parse(new ANTLRInputStream(stream));
            return new BuchiRuntime(declaration);
        }
    }
}
